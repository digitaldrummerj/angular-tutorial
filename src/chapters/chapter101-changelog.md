## Changelog

**8-5-2018**:

* Update: Angular to 6.1.2
* Update: bootstrap, ng-bootstrap, and fontawesome to latest versions
* Add: autocomplete attribute to email and password in Login and Signup pages