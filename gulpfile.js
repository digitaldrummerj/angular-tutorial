const { series, parallel, src, dest, watch } = require('gulp');

const plugins = {
	concat:  require('gulp-concat'),
  fileInclude:	 require('gulp-file-include'),
  marked: require('marked'),
  sass: require('gulp-sass'),
	webServer: require('gulp-webserver')
}

function include(cb) {
  return gulp
    .src('./src/*.html')
    .pipe(fileInclude({ prefix: '@@', filters: { markdown: marked } }))
    .pipe(gulp.dest('./'));
}

var build = series(include, sassTask, scripts);
var defaultTask = series(build, server, watchTask);

exports.build = build;
exports.default = defaultTask;
exports.sass = sassTask;
exports.include = include;
exports.scripts = scripts;
exports.server = server;
exports.watch = watchTask;

function watchTask() {
  return watch('./src/**/*', build);
}

function server() {
  return src('./').pipe(
    plugins.webServer({
      livereload: {
        enable: true,
        port: 35729,
        filter: function(fileName) {
          return fileName.match(/index.html$/);
        },
      },
      port: '3027',
      host: '0.0.0.0',
      //open: "http://localhost:3027"
    })
  );
}

function include() {
  return src('./src/*.html')
    .pipe(
      plugins.fileInclude({
        prefix: '@@',
        filters: {
          markdown: plugins.marked,
        },
      })
    )
    .pipe(dest('./'));
}

function sassTask() {
  return src('./src/scss/*.scss')
    .pipe(plugins.sass())
		.pipe(dest('./css'))
}

function scripts() {
  return src([
      'src/scripts/fastclick.js',
      'src/scripts/jquery.js',
      'src/scripts/highlight.pack.js',
      'src/scripts/app.js',
    ])
    .pipe(plugins.concat('built.js'))
    .pipe(dest('./scripts/'));
}
