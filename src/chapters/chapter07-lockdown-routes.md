## Locking Down Routes

### Overview

Right now in order to use our Todo list, we have to manually go to the login page first.  This is a terrible user experience.  Luckily, Angular has the ability to do checks before allowing a user into a route by implementing a guard on a route.

Guards allow you to authentication or authorization check before allowing a user to perform an action.  Common usage cases are:

* Role checks such as admin before allowing the user into the admin section of the application
* Is the user logged in check before allowing them into the page the pulls or updates data
* Is the user logged out before allowing them to create an account

In this chapter we are going to implement a check for if the user is logged in before allowing them to view the home page and interact with the Todo list. We will also be implementing a logout button.

### Goals

* Understand how to protect a route
* Understand how to use a guard

### Code from Previous Chapter

<div class="alert alert-danger" role="alert">Skip this section if you completed the previous chapter</div>

If you have not completed the previous chapter you can get the completed code by downloading the code from Github or open it in StackBlitz.

<h4 class="exercise-start">
    <b>Exercise</b>: Get Previous Code
</h4>

#### StackBlitz Online IDE

If you are using StackBlitz the previous chapter code is avavilable for StackBlitz at [https://stackblitz.com/github/digitaldrummerj/angular-tutorial-code/tree/chapter-reactive-forms](https://stackblitz.com/github/digitaldrummerj/angular-tutorial-code/tree/chapter-reactive-forms).

#### Downloading Code from Github

1. Downloading and extracting the zip file into your projects folder (c:\projects or ~/projects) at [https://github.com/digitaldrummerj/angular-tutorial-code/archive/chapter-reactvive-forms.zip](https://github.com/digitaldrummerj/angular-tutorial-code/archive/chapter-reactive-forms.zip)
1. After you get the code, run npm install to get all of the NPM dependencies.

    ```bash
    npm install
    ```

1. Open Visual Studio Code
1. In Visual Studio Code, go under the File menu, select Open folder and navigate to the folder that you unzipped the files into
1. If you have ng serve running in a different editor, make sure to stop it from running.
1. Open the Integrated Terminal in Visual Studio Code (ctrl + `)  and run ng serve

    ```bash
    ng serve
    ```

<div class="exercise-end"></div>

### Logout User

Before being able to signup for an account, we need the user to be logged out first.

There are a number of ways that you could implement this such as giving a logout button in the header or showing user info in header with link to profile page with a logout button.

We are going to implement the logout button in the header.

<h4 class="exercise-start">
    <b>Exercise</b>: Create AuthService Logout
</h4>

1. Open the auth.service.ts file

    ```bash
    auth.service.ts
    ```

1. Add the logout function below that will call the API logout function and clear out the cookie

    ```TypeScript
    logout(): Observable<boolean | Response> {
        return this.http
        .get('https://sails-ws.herokuapp.com/user/logout', requestOptions)
        .pipe(
            tap((res: Response) => {
                if (res.ok) {
                    return of(true);
                }

                return of(false);
           }),
            catchError((error: HttpErrorResponse) => {
                return of(false);
            })
        );
    }
   ```

<h4 class="exercise-start">
    <b>Exercise</b>: Add Logout Button
</h4>

For now we are just going to add the logout button on the screen but later in Chapter 9 we will move it to the header.

1. Open the src\app\shared\header\header.component.html

    ```bash
    app.component.html
    ```

1. After the `<div class="container"...` tag add the following div tag to call the logout service.  This link is just temporary until we implement a real header in a later lab.

    ```html
    <div class="nav-item active" (click)="logout()">
        <a class="nav-link" routerLink="">logout</a>
    </div>
    ```

<div class="exercise-end"></div>

<h4 class="exercise-start">
    <b>Exercise</b>: Add Component Logging Out Function
</h4>

1. Open app.component.ts

    ```bash
    app.component.ts
    ```

1. Import the AuthService and Router

    ```TypeScript
    import { Router } from '@angular/router';
    import { AuthService } from './shared/services/auth.service';
    ```


1. Add the Router to the constructor

    ```TypeScript
    constructor(private authService: AuthService, private router: Router) { }
    ```

1. Add the logout function

    ```TypeScript
    logout() {
        this.authService.logout().subscribe(() => {
            this.router.navigate(['/login']);
        });
    }
    ```

1. You are now ready to test it.

<div class="exercise-end"></div>

### Create Guard

<h4 class="exercise-start">
  <b>Exercise</b>: Check If User Is Logged In
</h4>

1. Open terminal and generate the guard

  ```bash
  ng generate guard shared/guards/IsLoggedIn
  ```

  ![generate output](images/isloggedin-generate.png)

<div class="exercise-end"></div>

### Add Logic to Guard

<h4 class="exercise-start">
  <b>Exercise</b>: Check If User Is Authenticated
</h4>

We need to make a call to the API to check if the user is logged into the API or not.  Since the login happens in the API, we need to check there and not just verify that we have the user info in the UI.

1. Open the src\app\shared\services\auth.service.ts file

  ```bash
  auth.service.ts
  ```

1. Add an isAuthenticated function to the AuthService that checks the API to make sure that the user is still logged in

  ```TypeScript
  isAuthenticated(): Observable<boolean | Response> {
        return this.http
            .get('https://sails-ws.herokuapp.com/user/identity', requestOptions)
            .pipe(
                tap((res: Response) => {
                    if (res) {
                        console.log('logged in');
                        return of(true);
                    }

                    console.log('not logged in');
                    return of(false);
                }),
                catchError((error: HttpErrorResponse) => {
                    if (error.status !== 403) {
                        console.log('isAuthenticated error', error);
                    }
                    console.log('not logged in', error);
                    return of(false);
                }),
            );
    }
  ```

1. Import HttpErrorResponse from @angular/common/http

  ```TypeScript
  import { HttpErrorResponse } from '@angular/common/http';
  ```

Next we need to add logic to the guard's canActivate function to call the AuthService.isAuthenticated function

1. Open src\app\shared\guards\is-logged-in.guard.ts

  ```bash
  is-logged-in.guard.ts
  ```

1. Import the AuthService

  ```TypeScript
  import { AuthService } from '../services/auth.service';
  ```

1. We are going to redirect our user to the home page if they are logged.  In order to do this we need to import the Router from @angular/route

  ```TypeScript
  import { Router } from '@angular/router';
  ```

1. Before we can use the AuthService and router we need to create a constructor and inject them into it

  ```TypeScript
  constructor(private authService: AuthService, private router: Router) { }
  ```

1. In the canActivate function we need to replace the return true with the following logic that will call the AuthService.isAuthenticated function and  return if the user is logged in or not.  If the user is not logged in or there is an error validating if they are logged in then we will navigate them to the login route else we will let them into the route

  ```TypeScript
    const isLoggedIn = new Observable<boolean>(observer => {
    this.authService.isAuthenticated()
      .subscribe((res: boolean) => {
        if (res) {
          observer.next(true);
          observer.complete();
        } else {
          this.router.navigate(['/login']);
          observer.next(false);
          observer.complete();
        }
      }, error => {
        this.router.navigate(['/login']);
        observer.next(false);
        observer.complete();
      });
    });

    return isLoggedIn;
  ```

<div class="exercise-end"></div>

### Add Guard to Route

In order to use the Guard we need to add it to the route.  Each route has a canActivate attribute that takes an array of guards as the 3rd parameter.

<h4 class="exercise-start">
  <b>Exercise</b>: Add Guard to Route
</h4>

1. Open the app-routing.module.ts file

  ```bash
  app-routing.module.ts
  ```

1. Import the IsLoggedInGuard

  ```TypeScript
  import { IsLoggedInGuard} from './shared/guards/is-logged-in.guard';
  ```

1. To the default route, add the canActivate attribute with the value being an array that contains the IsLoggedInGuard

  ```TypeScript
  canActivate: [IsLoggedInGuard],
  ```

1. You route should look like

  ```TypeScript
  {
    path: '',
    component: TodoComponent,
    canActivate: [IsLoggedInGuard],
  },
  ```

1. Now when you try to go to [http://localhost:4200](http://localhost:4200) if you are not already logged in it will redirect you to the login page.

1. If you are already logged in and want to test the guard, click on the logout button and then navigate to [http://localhost:4200](http://localhost:4200) and you should be redirected to the login page.

<div class="exercise-end"></div>
