## Deploying

### Overview

Before deploy you will need to run an a build with the Angular CLI.  With the build you can tell it the environment to use and if it is a production build or not.  A production will minify everything.

### Goals

* Learn how to create a build that is ready to deploy.

### Code from Previous Chapter

<div class="alert alert-danger" role="alert">Skip this section if you completed the previous chapter</div>

If you have not completed the previous chapter you can get the completed code by downloading the code from Github or open it in StackBlitz.

<h4 class="exercise-start">
    <b>Exercise</b>: Get Previous Code
</h4>

#### StackBlitz Online IDE

If you are using StackBlitz the previous chapter code is avavilable for StackBlitz at [https://stackblitz.com/github/digitaldrummerj/angular-tutorial-code/tree/chapter-env-config](https://stackblitz.com/github/digitaldrummerj/angular-tutorial-code/tree/chapter-env-config).

#### Downloading Code from Github

1. Downloading and extracting the zip file into your projects folder (c:\projects or ~/projects) at [https://github.com/digitaldrummerj/angular-tutorial-code/archive/chapter-env-config.zip](https://github.com/digitaldrummerj/angular-tutorial-code/archive/chapter-env-config.zip)
1. After you get the code, run npm install to get all of the NPM dependencies.

    ```bash
    npm install
    ```

1. Open Visual Studio Code
1. In Visual Studio Code, go under the File menu, select Open folder and navigate to the folder that you unzipped the files into
1. If you have ng serve running in a different editor, make sure to stop it from running.
1. Open the Integrated Terminal in Visual Studio Code (ctrl + `)  and run ng serve

    ```bash
    ng serve
    ```

<div class="exercise-end"></div>

### Building Steps

In order to deploy you should run through unit test, linting, end to end testing, and building for the environment.

### Linting

<h4 class="exercise-start">
  <b>Exercise</b>: Linting The Code
</h4>

TSLint comes pre-configured with the project that the Angular CLI created.  To run linting in a CI environment, run

  ```bash
  ng lint
  ```

<div class="alert alert-info" role="alert">If you are using TeamCity for your CI you will want to install the tslint-teamcity-reporter package (`npm install --save-dev tslint-teamcity-reporter`) and change the lint command to `ng lint --format tslint-teamcity-reporter`</div>

<div class="exercise-end"></div>

### Testing

<h4 class="exercise-start">
  <b>Exercise</b>: Unit Testing the Code
</h4>

<div class="alert alert-danger" role="alert">This step will fail as we have not been updating the unit tests. In your own projects outside the workshop, you would want to keep the unit tests updated as you go </div>

1. Open a terminal and navigate to your Angular project
1. Run the following command to run unit testing one time with code coverage using the Chrome Headless browser

  ```bash
  ng test --watch=false --code-coverage=true --browsers ChromeHeadless
  ```

<div class="exercise-end"></div>

### End to End Testing

<h4 class="exercise-start">
  <b>Exercise</b>: End to End Testing Your Code
</h4>

Before we can run our end to end test, we need to create a configuration file to ensure that we run using the Chrome headless browser and set the browser resolution to use.

1. In the e2e directory at the root of the project, create a file called protractor.conf.ci.js

  ```bash
  protractor.conf.ci.js
  ```

1. Set the contents of the `protractor.conf.ci.js` to the following.  The difference between this file and the protractor.conf.js file is that chromeOptions section.

  ```javascript
  // Protractor configuration file, see link for more information
  // https://github.com/angular/protractor/blob/master/lib/config.ts

  const { SpecReporter } = require('jasmine-spec-reporter');

  exports.config = {
    allScriptsTimeout: 11000,
    specs: [
      './src/**/*.e2e-spec.ts'
    ],
    capabilities: {
      browserName: 'chrome',
      chromeOptions: {
        args: ['--headless', '--disable-gpu', '--window-size=1280,768'],
      },
    },
    directConnect: true,
    baseUrl: 'http://localhost:4200/',
    framework: 'jasmine',
    jasmineNodeOpts: {
      showColors: true,
      defaultTimeoutInterval: 30000,
      print: function() {},
    },
    onPrepare() {
      require('ts-node').register({
        project: require('path').join(__dirname, './tsconfig.e2e.json')
      });
      jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
    },
  };
  ```

1. Open a terminal and navigate to your Angular project
1. To run end to end tests in a CI environment, run

  ```bash
  ng e2e --protractor-config e2e/protractor.conf.ci.js
  ```

Now that the works, you can update the `angular.json` file to have a ci configuration where you use this file and set ng serve to be the production build

1. Open the angular.json file at the root of the project

  ```bash
  angular.json
  ```

1. Find the `ngws-e2e` section in the file and then find the architect -> configurations section and we are going to add a 2nd configuration setting

  ```json
  "ci": {
    "devServerTarget": "ngws:serve:production",
    "protractorConfig": "e2e/protractor.conf.ci.js"
  }
  ```

1. Now if we go back to the terminal we can run our new configuration

  ```bash
  ng e2e -c ci
  ```

<div class="exercise-end"></div>

### Creating Non-Production Build

<div class="alert alert-info" role="alert">
Note the build create a dist folder to hold the output.   This directory is removed before each build
</div>

<h4 class="exercise-start">
  <b>Exercise</b>: Running Non-Production Build
</h4>

Run the following command to run a build that uses the environment.ts file and is not a production build

```bash
ng build
```

<div class="exercise-end"></div>

### Creating Production Build

<h4 class="exercise-start">
  <b>Exercise</b>: Running a Production Build
</h4>

<div class="alert alert-warning" role="alert">
Note that this will remove the dist directory before build.
</div>

Run the following command to run a build that uses the environment.prod.ts file and is a production build.

```bash
ng build --prod
```

You will now have a production ready deploy in the the dist folder.

<div class="exercise-end"></div>

### Make It Easy to Remember Commands

It can be a pain to remember all of these commands.  I like to update the package.json scripts section to have a script for each of the sections.

1. Open the package.json file

  ```bash
  package.json
  ```

1. Find the scripts section and replace it with the following section

  ```json
  "scripts": {
      "ng": "ng",
      "start": "ng serve",
      "build": "ng build",
      "build:prod": "ng build --prod",
      "test": "ng test",
      "test:ci": "ng test --watch=false --code-coverage=true --browsers ChromeHeadless",
      "lint": "ng lint",
      "lint:ci": "ng lint",
      "e2e": "ng e2e",
      "e2e:ci": "ng e2e -c ci",
      "ci": "npm run lint:ci && npm run test:ci && npm run e2e:ci && npm run build:prod"
    },
  ```

  > **Note 1:** The ci script is meant to be used locally before pushing to version control

  > **Note 2:** In your CI environment, make sure to run each step individually so that you know which step actually failed.  If you run `npm run ci` and a step fails, many times it is hard to easily tell which command actually failed.

  > **Note 3:** To run any of these commands you run `npm run` and then the name of the script.  For example, to run the lint:ci script you would run `npm run lint:ci`
